//Class
class Person{
    constructor(name, age, salary,sex){
        this.name = name
        this.age = age
        this.salary = salary
        this.sex = sex
    }

    static sortByFeild(persons, field, order='asc'){
        const partition = (persons, field, low, high, order)=>{
            const pivot = persons[high][field]
            let i = low -1
            for (let j = low ; j<=high-1; j++){
                if (order === 'asc'){
                    if (persons[j][field] < pivot){
                        i+=1
                        if (persons[j][field] !== persons[i][field]){
                            const temp = persons[j]
                            persons[j] = persons[i]
                            persons[i] = temp
                        }
                    }
                }else if(order === 'desc'){
                    if (persons[j][field] > pivot){
                        i+=1
                        if (persons[j][field] !== persons[i][field]){
                            const temp = persons[j]
                            persons[j] = persons[i]
                            persons[i] = temp
                        }
                    }
                }
            }

            const temp = persons[i+1]
            persons[i+1] = persons[high]
            persons[high] = temp

            return i+1
        }
        const quickSort = (persons, field, low, high, order)=>{
            console.log(low, high)
            if (low < high){
                let pos = partition(persons, field, low, high, order)
                console.log(pos)
                quickSort(persons, field, pos+1, high, order)
                quickSort(persons, field, low, pos-1, order)
            }
        }

        let low = 0
        let high = persons.length -1
        quickSort(persons, field, low, high, order)
    }
}

const mem1 = new Person('Aishabh', 10, 40000, 'Male')
const mem2 = new Person('Bomething', 80, 8700, 'Male')
const mem3 = new Person('Cishabh', 30, 40000, 'Male')
const mem4 = new Person('Domething', 90, 8700, 'Male')
const mem5 = new Person('Eishabh', 40, 40000, 'Male')
const mem6 = new Person('Fomething', 50, 8700, 'Male')
const persons = [mem1, mem3, mem4, mem2, mem5, mem6]
Person.sortByFeild(persons, 'name', 'desc')
console.log(persons)